import com.cronutils.model.Cron;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinition;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.model.time.ExecutionTime;
import com.cronutils.parser.CronParser;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Deze klasse biedt functionaliteit rond het parsen van Cron-strings.
 * De gebruikte standaard is de Quartz definitie.
 * (Zie https://www.quartz-scheduler.net/documentation/quartz-3.x/tutorial/crontriggers.html)
 *
 * Handige tool om een Cron-string te bouwen:
 * https://www.freeformatter.com/cron-expression-generator-quartz.html
 */
public class CronLezer {

    private final CronParser lezer;

    public CronLezer() {
        CronDefinition cronDefinitie = CronDefinitionBuilder.instanceDefinitionFor(CronType.QUARTZ);
        this.lezer = new CronParser(cronDefinitie);
    }


    /**
     * Retourneert of de gegeven Cron-string vuurt op de gevraagde datum.
     * @param cronString een Cron-string volgens het Quartz-formaat.
     * @param datum een LocalDate object
     * @return true als de Cron-string vuurt op de gevraagde datum,
     *         anders false.
     */
    public boolean vuurtOpDatum(String cronString, LocalDate datum) {

        Cron cron = parseCron(cronString);

        // We hebben ZonedDateTimes nodig, dus die maken we mbv de default zone-id van het systeem.
        ZonedDateTime startVanDag = ZonedDateTime.of(datum.atStartOfDay(), ZoneId.systemDefault());
        ZonedDateTime startVanVolgendeDag = startVanDag.plusDays(1);

        ExecutionTime executieTijd = ExecutionTime.forCron(cron);

        // Return true bij een exacte match (dus de cron vuurt precies op 00:00 van de gevraagde datum).
        if (executieTijd.isMatch(startVanDag))
            return true;

        // Zo niet, dan kijken we of de volgende trigger plaatsvindt voor 00:00 van de volgende dag.
        Optional<ZonedDateTime> volgendeTrigger = executieTijd.nextExecution(startVanDag);
        if (volgendeTrigger.isEmpty())
            return false;

        return volgendeTrigger.get().isBefore(startVanVolgendeDag);
    }

    private Cron parseCron(String cronString) {
        try {
            return lezer.parse(cronString);
        } catch(IllegalArgumentException ex) {
            // TODO: reraise als IOA configuratie error?
            // Of laten we die verantwoordelijkheid aan de aanroeper van deze klasse?
            throw ex;
        }
    }
}
