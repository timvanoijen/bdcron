import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;


class CronLezerTest {

    // To be injected?
    private final CronLezer cut = new CronLezer();

    private final LocalDate now = LocalDate.now();

    @Test
    void testDagelijkseTrigger() {
        String cronString = "0 0 7 1/1 * ? *"; // Dagelijks om 7:00.
        assert cut.vuurtOpDatum(cronString, now);
        assert cut.vuurtOpDatum(cronString, now.plusDays(1));
        assert cut.vuurtOpDatum(cronString, now.plusDays(-1));
    }

    @Test
    void testDagelijkseTriggerExacteMatchMiddernacht() {
        String cronString = "0 0 0 1/1 * ? *"; // Dagelijks om 00:00
        assert cut.vuurtOpDatum(cronString, now);
        assert cut.vuurtOpDatum(cronString, now.plusDays(1));
        assert cut.vuurtOpDatum(cronString, now.plusDays(-1));
    }

    @Test
    void testWekelijkseTriggerOpMaandag() {
        String cronString = "0 0 0 ? * MON *"; // Elke maandag
        assert cut.vuurtOpDatum(cronString, maakLocalDateVoorDagInWeek(now, DayOfWeek.MONDAY));
        assert !cut.vuurtOpDatum(cronString, maakLocalDateVoorDagInWeek(now, DayOfWeek.TUESDAY));
        assert !cut.vuurtOpDatum(cronString, maakLocalDateVoorDagInWeek(now, DayOfWeek.SUNDAY));
    }

    @Test
    void testTriggerOpAlleWerkdagen() {
        String cronString = "0 0 0 ? * MON,TUE,WED,THU,FRI *"; // Elke werkdag
        assert cut.vuurtOpDatum(cronString, maakLocalDateVoorDagInWeek(now, DayOfWeek.THURSDAY));
        assert cut.vuurtOpDatum(cronString, maakLocalDateVoorDagInWeek(now, DayOfWeek.TUESDAY));
        assert !cut.vuurtOpDatum(cronString, maakLocalDateVoorDagInWeek(now, DayOfWeek.SATURDAY));
        assert !cut.vuurtOpDatum(cronString, maakLocalDateVoorDagInWeek(now, DayOfWeek.SUNDAY));
    }

    @Test
    void testTriggerOp1eEn15eVanDeMaand() {
        String cronString = "0 0 0 1,15 * ? *"; // Elke 1e en 15e van de maand
        assert cut.vuurtOpDatum(cronString, LocalDate.of(2021, 9, 1));
        assert cut.vuurtOpDatum(cronString, LocalDate.of(2021, 9, 15));
        assert !cut.vuurtOpDatum(cronString, LocalDate.of(2021, 9, 12));
        assert !cut.vuurtOpDatum(cronString, LocalDate.of(2021, 9, 16));
    }

    @Test
    void testTriggerOpDichtstbijzijndeWerkdagVoorStartVanDeMaand() {
        String cronString = "0 0 0 1W * ? *"; // Dichtstbijzijnde werkdag bij 1e van de maand
        assert cut.vuurtOpDatum(cronString, LocalDate.of(2021, 9, 1)); // Woensdag
        assert cut.vuurtOpDatum(cronString, LocalDate.of(2021, 8, 2)); // Maandag
        assert !cut.vuurtOpDatum(cronString, LocalDate.of(2021, 8, 1)); // Zondag
    }

    @Test
    void testWrongCronString() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> cut.vuurtOpDatum("stringwitherror", now));
    }

    private LocalDate maakLocalDateVoorDagInWeek(LocalDate datum, DayOfWeek dagVanWeek) {
        DayOfWeek huidigeDagVanWeek = datum.getDayOfWeek();
        return datum.plusDays(dagVanWeek.getValue() - huidigeDagVanWeek.getValue());
    }
}